from multiprocessing import Process, Queue
from datetime import datetime, timezone
import traceback
import asyncio
import queue
import os

from discord.ext import commands
import ruamel.yaml as yaml
import dateutil.parser
import feedparser
import discord

from .util.checks import right_channel
from .util.yt_site import yt_site


OP_ADD_NEW = 0
OP_SEND_NOTIF = 1


class YouTube:
    BASE_URL = 'http://youtube.com/feeds/videos.xml?'
    MAX_ATTEMPTS = 5
    RETRY_AFTER = 10
    LOOP_DELAY = 240  # 4 minutes

    def __init__(self, bot):
        self.bot = bot
        self.task = self.bot.loop.create_task(self.youtube_feed())

        self.config = {}
        with open('config/youtube.yml', 'r') as f:
            self.config = yaml.load(f, Loader=yaml.Loader)

        # Create our seperate process, queue, and queue monitor
        self.site_queue = Queue()
        self.site_process = Process(target=yt_site, args=(self.site_queue,))
        self.site_process.start()

        self.site_task = self.bot.loop.create_task(self.site_checker())

    def __unload(self):
        # Kill all the things
        self.task.cancel()
        self.site_task.cancel()
        self.site_process.terminate()

    def save_config(self):
        with open('config/youtube.yml', 'w') as f:
            yaml.dump(self.config, f)

    async def site_checker(self):
        while True:
            # Check for new commands on the queue
            if self.site_queue.empty():
                await asyncio.sleep(0.25)
                continue

            op, args = self.site_queue.get()

            print(f'NEW MSG: {op} | {args}')

            if op == OP_ADD_NEW:
                # Add new channel
                guild, channel = args
                self.config[guild]['feeds'].append(channel)
                self.save_config()

                self.bot.logger.info(f'Added channel {channel} to guild {guild}!')
            elif op == OP_SEND_NOTIF:
                channel, msg = args
                channel = self.bot.get_channel(channel)
                await channel.send(msg)

    def _get_feeds(self):
        """Build a nicer structure for handling later on"""
        feeds = set()
        channels = {}

        for guild in self.config:
            for feed in self.config.get(guild, {}).get('feeds', {}):
                feed = feed.replace('channel=', 'channel_id=')
                url = self.BASE_URL + feed
                feeds.add(url)
                channels[url] = channels.get(url, set())
                channels[url].add(self.config[guild].get('channel'))
        return list(feeds), channels

    async def _check_feeds(self, urls):
        feeds, channels = self._get_feeds()

        for feed_url in feeds:
            self.bot.logger.debug(f'Checking {feed_url}')

            async with self.bot.session.get(feed_url) as resp:
                feed = feedparser.parse(await resp.read())

            videos = feed['entries']

            for video in videos:
                href = video['link']

                if href not in urls:
                    urls.append(href)

                    pub = dateutil.parser.parse(video['published'])
                    if (datetime.now(timezone.utc) - pub).days > 1:
                        self.bot.logger.info(f'Video {video["title"]} too old. Skipping.')
                        continue

                    title = video['title']
                    self.bot.logger.info(f'New video: {title} - {href}')

                    for channel in channels.get(feed_url, []):
                        if channel is None:
                            continue

                        channel = self.bot.get_channel(channel)
                        if channel is None:
                            continue

                        await channel.send(f':film_frames: {href}')

        with open('state_cache/videoURLS.txt', 'w') as f:
            f.write('\n'.join(urls))

        await asyncio.sleep(self.LOOP_DELAY)

        return urls

    async def youtube_feed(self):
        await self.bot.wait_until_ready()
        if not os.path.exists('state_cache/videoURLS.txt'):
            os.mknod('state_cache/videoURLS.txt')

        with open('state_cache/videoURLS.txt') as f:
            urls = f.read().split('\n')

        feeds, _ = self._get_feeds()
        self.bot.logger.info(f'Located {len(feeds)} feeds')

        attempts = 0
        while True:
            try:
                urls = await self._check_feeds(urls)
            except:
                self.bot.logger.error('Something went wrong checking feeds. '\
                                      'Traceback below:')
                traceback.print_exc()
                await asyncio.sleep(self.RETRY_AFTER)
                attempts += 1
            else:
                attempts = 0

            if attempts > self.MAX_ATTEMPTS:
                self.bot.logger.error('MAX_ATTEMPTS hit. Giving up.')
                break


def setup(bot):
    bot.add_cog(YouTube(bot))
